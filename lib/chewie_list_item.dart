import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:video_player/video_player.dart';

class ChewieListItem extends StatefulWidget {

  final VideoPlayerController videoPlayercontroller;
  final bool looping;

  ChewieListItem({
    @required this.videoPlayercontroller,
    this.looping,
    Key key,
  }) : super(key: key);

  @override
  _ChewieListItemState createState() => _ChewieListItemState();
}

class _ChewieListItemState extends State<ChewieListItem> {
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
        _chewieController = ChewieController(
      videoPlayerController: widget.videoPlayercontroller,
      aspectRatio: 16 / 9,
      autoInitialize: true,
      looping: widget.looping,
      errorBuilder: (context, errorMessege) {
        return Center(
          child: Text(
            errorMessege,
            style: TextStyle(color: Colors.amber)
          ),
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:Chewie(
        controller: _chewieController,
      )
    );
  }

  @override
  void dispose() {
    super.dispose();
    widget.videoPlayercontroller.dispose();
    _chewieController.dispose();
  }
}