import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';
import 'chewie_list_item.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Look a video!',
      theme: ThemeData(
        primarySwatch: Colors.green,

      ),

      home: MyHomePage(
        
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video!')
      ),
      body: ListView(
        children: <Widget>[
          ChewieListItem(
            videoPlayercontroller: VideoPlayerController.asset(
              'videos/videoplayback.mp4',
            ),
            looping: true,
          )
        ]
      ),
    );
  }
} 